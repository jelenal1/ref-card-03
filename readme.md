
# Refcard03

Refcard03 ist ein Java 11 Spring Boot Projekt, das es Benutzern ermöglicht, Witze zu lesen.

## CI/CD-Konfiguration

Um die CI/CD-Pipeline für das Refcard03-Projekt einzurichten, müssen Sie die folgenden Schritte ausführen:

1. Stellen Sie sicher, dass Sie ein GitLab-Konto haben und über die erforderlichen Zugriffsrechte für das Repository verfügen.
2. Erstellen Sie eine `.gitlab-ci.yml`-Datei im Stammverzeichnis des Projekts. Diese Datei enthält die Pipeline-Konfigurationsschritte, die Sie bereits erstellt haben. Passen Sie die `.gitlab-ci.yml`-Datei an Ihre spezifischen Anforderungen an.
3. Konfigurieren Sie die erforderlichen CI/CD-Variablen in Ihrem GitLab-Projekt:
   * `CI_AWS_ECR_REGISTRY`: Die URL des AWS Elastic Container Registry (ECR), in dem das Docker-Image gespeichert wird.
   * `CI_AWS_ECR_REPOSITORY_NAME`: Der Name des ECR-Repositories, in dem das Docker-Image gespeichert wird.
4. Stellen Sie sicher, dass Sie über die erforderlichen Umgebungsvariablen für die AWS Elastic Container Service (ECS) verfügen. Diese Variablen werden benötigt, um das Refcard03-Image in der ECS-Umgebung bereitzustellen. Die spezifischen Umgebungsvariablen können je nach Ihren Anforderungen variieren, können aber beispielsweise Folgendes umfassen:
   * `DB_URL`: Die URL der MariaDB-Datenbank.
   * `DB_USERNAME`: Der Benutzername für den Datenbankzugriff.
   * `DB_PASSWORD`: Das Passwort für den Datenbankzugriff.
   * `AWS_ACCESS_KEY_ID`: Der Zugriffsschlüssel für den AWS-Dienst.
   * `AWS_SECRET_ACCESS_KEY`: Der geheime Zugriffsschlüssel für den AWS-Dienst.
   * `AWS_REGION`: Die AWS-Region, in der sich Ihre Ressourcen befinden.
   * Weitere Umgebungsvariablen, die für Ihre Anwendung spezifisch sind.
5. Stellen Sie sicher, dass die CI/CD-Pipeline in Ihrem GitLab-Projekt konfiguriert und aktiviert ist. GitLab wird die Pipeline automatisch ausführen, wenn Änderungen im Repository vorgenommen werden.
