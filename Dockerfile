FROM maven:3-openjdk-11-slim
COPY src /src
COPY pom.xml /
RUN mvn -f pom.xml clean package
RUN mv /target/*.jar app.jar
EXPOSE 3000
ENTRYPOINT ["java","-jar","/app.jar"]
